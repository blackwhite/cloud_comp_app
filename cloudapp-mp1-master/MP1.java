import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;



public class MP1 {
	
	Random generator;
    String userName;
    String inputFileName;
    String delimiters = " \t,;.?!-:@[](){}_*/";
    String[] stopWordsArray = {"i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours",
            "yourself", "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its",
            "itself", "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that",
            "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had", "having",
            "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as", "until", "while",
            "of", "at", "by", "for", "with", "about", "against", "between", "into", "through", "during", "before",
            "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off", "over", "under", "again",
            "further", "then", "once", "here", "there", "when", "where", "why", "how", "all", "any", "both", "each",
            "few", "more", "most", "other", "some", "such", "no", "nor", "not", "only", "own", "same", "so", "than",
            "too", "very", "s", "t", "can", "will", "just", "don", "should", "now"};

    void initialRandomGenerator(String seed) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA");
        messageDigest.update(seed.toLowerCase().trim().getBytes());
        byte[] seedMD5 = messageDigest.digest();

        long longSeed = 0;
        for (int i = 0; i < seedMD5.length; i++) {
            longSeed += ((long) seedMD5[i] & 0xffL) << (8 * i);
        }

        this.generator = new Random(longSeed);
    }

    Integer[] getIndexes() throws NoSuchAlgorithmException {
        Integer n = 10000;
        Integer number_of_lines = 50000;
        Integer[] ret = new Integer[n];
        this.initialRandomGenerator(this.userName);
        for (int i = 0; i < n; i++) {
            ret[i] = generator.nextInt(number_of_lines);
        }
        return ret;
    }

    public MP1(String userName, String inputFileName) {
        this.userName = userName;
        this.inputFileName = inputFileName;
    }
    
    public String[] process() throws Exception {
        String[] ret = new String[20];
        
        Map<String, WordStat> words = new HashMap<String, WordStat>();
        WordStat ws;
        List<String> ignoreTokens = Arrays.asList(this.stopWordsArray);
        String sentence = null;
        List<String> tokens = null;
        StringTokenizer tokenizer = null;
        List<String> sentences = this.getAllSentences(this.inputFileName);
        List<Integer> indexesASC = Arrays.asList(this.getIndexes());
        
        Collections.sort(indexesASC);
        
        for (int i = 0; i < indexesASC.size(); i++) {
        	sentence = sentences.get(indexesASC.get(i));
        	
        	sentence = sentence.toLowerCase().trim();
        	
            tokenizer = new StringTokenizer(sentence);       	
        	tokens = new ArrayList<String>();
        	
            while (tokenizer.hasMoreTokens()) {
            	tokens.add(tokenizer.nextToken(this.delimiters));
            }
            
            for (String token : tokens) {
            	
            	if (!ignoreTokens.contains(token)) {
            		if (words.containsKey(token)) {
            			ws = words.get(token);
            			ws.incrementCount();
            		} else {
            			ws = new WordStat(token, 1);
            			words.put(token, ws);
            		}
            	}
            }            
        }
        
        ArrayList<WordStat> wordsStats = new ArrayList<WordStat>(words.values());
        Collections.sort(wordsStats);
        Collections.reverse(wordsStats);
        
        for (int i=0; i < ret.length; i++) {
        	ret[i] = wordsStats.get(i).getWord();
        }
        
        return ret;
    }
    
    public List<String> getAllSentences(String filename) {
    	if (filename == null || filename.equals("")) {
            return null;
        }
        ArrayList<String> sentences = new ArrayList<String>();
        BufferedReader reader;
        String line;
        
        try {
        	reader = new BufferedReader(new FileReader(filename));
            line = reader.readLine();
			
            while (line != null) {
            	sentences.add(line);
            	line = reader.readLine();
            }
	        reader.close();	
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
    	
    	return sentences;
    }

	public static void main(String[] args) throws Exception {
		if (args.length < 1){
            System.out.println("MP1 <User ID>");
        }
        else {
            String userName = args[0];
            String inputFileName = "./input.txt";
            MP1 mp = new MP1(userName, inputFileName);
            String[] topItems = mp.process();
            for (String item: topItems){
                System.out.println(item);
            }
        }

	}
	
private class WordStat implements Comparable<WordStat> {
    	
    	private String word;
    	private int count;
    	
    	public WordStat(String w, int count) {
    		this.word = w;
    		this.count = count;
    	}
    	
    	public void incrementCount() {
    		this.count++;
    	}
		
    	@Override
		public int compareTo(WordStat word2) {
    		
    		if ((this.word.equals("river") & word2.getWord().equals("c")) || 
    				(this.word.equals("c") & word2.getWord().equals("river"))) {
    			System.out.println("ddd");
    			
    		}
    		if (this.getCount() < word2.getCount()) return -1;
    		else if (this.getCount() > word2.getCount()) return 1;
    		
			
			if ((int) this.word.charAt(0) < (int) word2.getWord().charAt(0))
				return +1;
			else if ((int) this.word.charAt(0) > (int) word2.getWord().charAt(0))
				return -1;
			
			if (this.word.length() > word2.getWord().length())
				return -1;
			else if (this.word.length() < word2.getWord().length())
			    return +1;
			
			return 0;
		}
    	
    	public String getWord() { return word ;}
    	public int getCount() { return count; }
    	public void setCount(int newCount) { this.count = newCount;}  
    	
    	public String toString() {
    		return this.getWord() + " - " + this.count;
    	}
    	
    }

}
