import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.classification.SVMModel;
import org.apache.spark.mllib.classification.SVMWithSGD;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.tree.model.RandomForestModel;
import org.apache.spark.mllib.tree.RandomForest;
import org.apache.spark.mllib.util.MLUtils;


import java.util.HashMap;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public final class RandomForestMP {

    private static Logger logger = Logger.getLogger(RandomForest.class.getName());

    private static class ParseTrainningData implements Function<String, LabeledPoint> {
        private static final Pattern padrao = Pattern.compile(",");

        public LabeledPoint call(String line) {
            String[] tokens = padrao.split(line);
            double[] atributos = new double[tokens.length - 1];

            for (int i=0; i < tokens.length -1; i++) {
                atributos[i] = Double.parseDouble(tokens[i]);

            }
            logger.info("Trainning:" + tokens[tokens.length -1] + " - " + Vectors.dense(atributos).toString());
            return new LabeledPoint(Double.parseDouble(tokens[tokens.length -1]), Vectors.dense(atributos));
        }
    }

    private static class ParseTestData implements Function<String, Vector> {
        private static Pattern padrao = Pattern.compile(",");

        public Vector call(String line) {
            String[] tokens = padrao.split(line);
            double[] atributos = new double[tokens.length - 1];

            for (int i=0; i < tokens.length -1; i++) {
                atributos[i] = Double.parseDouble(tokens[i]);

            }

            logger.info("Test:" + Vectors.dense(atributos).toString());
            return Vectors.dense(atributos);
        }
    }

    public static void main(String[] args) {
        if (args.length < 3) {
            System.err.println(
                    "Usage: RandomForestMP <training_data> <test_data> <results>");
            System.exit(1);
        }
        String training_data_path = args[0];
        String test_data_path = args[1];
        String results_path = args[2];

        SparkConf sparkConf = new SparkConf().setAppName("RandomForestMP");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        final RandomForestModel model;

        Integer numClasses = 2;
        HashMap<Integer, Integer> categoricalFeaturesInfo = new HashMap<Integer, Integer>();
        Integer numTrees = 3;
        String featureSubsetStrategy = "auto";
        String impurity = "gini";
        Integer maxDepth = 5;
        Integer maxBins = 32;
        Integer seed = 12345;

		// TODO
        JavaRDD<String> linhasDadosTreinamento = sc.textFile(training_data_path);
        JavaRDD<String> linhasDadosTeste = sc.textFile(test_data_path);

        JavaRDD<LabeledPoint> dadosTreinamento = linhasDadosTreinamento.map(new ParseTrainningData());
        JavaRDD<Vector> dadosTeste = linhasDadosTeste.map(new ParseTestData());

        model = RandomForest.trainClassifier(dadosTreinamento, numClasses, categoricalFeaturesInfo,
                numTrees, featureSubsetStrategy, impurity, maxDepth, maxBins, seed);

        JavaRDD<LabeledPoint> results = dadosTeste.map(new Function<Vector, LabeledPoint>() {
            public LabeledPoint call(Vector points) {
                return new LabeledPoint(model.predict(points), points);
            }
        });

        results.saveAsTextFile(results_path);

        sc.stop();
    }

}
