import java.util.regex.Pattern;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import scala.Tuple2;


public final class KMeansMP {

    private static class ParseAttributes implements Function<String, Vector> {
        private static final Pattern padrao = Pattern.compile(",");

        public Vector call(String line) {
            String[] tokens = padrao.split(line);
            double[] atributos = new double[tokens.length -1];

            for (int i=1; i < tokens.length; i++) {
                atributos[i-1] = Double.parseDouble(tokens[i]);
            }
            return Vectors.dense(atributos);
        }
    }

    private static class ParseClass implements Function<String, String> {
        private static Pattern padrao = Pattern.compile(",");

        public String call(String line) {
            String[] tokens = padrao.split(line);

            return tokens[0];
        }
    }

    private static class ClustersData implements PairFunction<Tuple2<String, Vector>, Integer, String> {
        private KMeansModel modelo;

        public ClustersData(KMeansModel modelo) {
            this.modelo = modelo;
        }

        public Tuple2<Integer, String> call(Tuple2<String, Vector> args) {
            String classe = args._1();
            Vector atributos = args._2();
            int cluster = modelo.predict(atributos);

            return new Tuple2<Integer, String>(cluster, classe);
        }
    }
    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println(
                    "Usage: KMeansMP <input_file> <results>");
            System.exit(1);
        }
        String inputFile = args[0];
        String results_path = args[1];
        JavaPairRDD<Integer, Iterable<String>> results;
        int k = 4;
        int iterations = 100;
        int runs = 1;
        long seed = 0;
		final KMeansModel model;
		
        SparkConf sparkConf = new SparkConf().setAppName("KMeans MP");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);

        JavaRDD<String> linhas = sc.textFile(inputFile);
        JavaRDD<Vector> atributos = linhas.map(new ParseAttributes());
        JavaRDD<String> classes = linhas.map(new ParseClass());

        model = KMeans.train(atributos.rdd(), k, iterations, runs, KMeans.RANDOM(), seed);

        results = classes.zip(atributos).mapToPair(new ClustersData(model)).groupByKey();


        results.saveAsTextFile(results_path);

        sc.stop();
    }
}