import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.conf.LongConfOption;
import org.apache.giraph.edge.Edge;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * Compute shortest paths from a given source.
 */
public class ShortestPathsComputation extends BasicComputation<
    IntWritable, IntWritable, NullWritable, IntWritable> {
  /** The shortest paths id */
  public static final LongConfOption SOURCE_ID =
      new LongConfOption("SimpleShortestPathsVertex.sourceId", 1,
          "The shortest paths id");

    private static Logger logger = Logger.getLogger(ShortestPathsComputation.class.getName());

  /**
   * Is this vertex the source id?
   *
   * @param vertex Vertex
   * @return True if the source id
   */
  private boolean isSource(Vertex<IntWritable, ?, ?> vertex) {
    return vertex.getId().get() == SOURCE_ID.get(getConf());
  }

  @Override
  public void compute(
      Vertex<IntWritable, IntWritable, NullWritable> vertex,
      Iterable<IntWritable> messages) throws IOException {

      if (this.getSuperstep() == 0) {
          if (this.isSource(vertex)) vertex.setValue(new IntWritable(0));
          vertex.setValue(new IntWritable(Integer.MAX_VALUE));
      }
      int minValorMensagem = this.isSource(vertex) ? 0 : Integer.MAX_VALUE;

      for (IntWritable mensagem : messages) {
          minValorMensagem = Math.min(minValorMensagem, mensagem.get());
      }
      logger.info("--------INI--------" + this.getSuperstep());
      System.out.println("--------INI--------" + this.getSuperstep());
      logger.info("VID: " + vertex.getId().get() + " - " + "VValue: " + vertex.getValue().get() +
              "minValorMensagem:" + minValorMensagem);
      System.out.println("VID: " + vertex.getId().get() + " - " + "VValue: " + vertex.getValue().get() +
              "minValorMensagem:" + minValorMensagem);
      logger.info("--------FIM--------" + this.getSuperstep());
      System.out.println("--------FIM--------" + this.getSuperstep());


      if (minValorMensagem < vertex.getValue().get()) {
          vertex.setValue(new IntWritable(minValorMensagem));

          int valorAresta;
          int valorMin;
          int valorVertice = vertex.getValue().get();

          for (Edge<IntWritable, NullWritable> aresta : vertex.getEdges()) {

             valorMin = Math.min(valorVertice, aresta.getTargetVertexId().get());

             if (valorMin < aresta.getTargetVertexId().get()) {
                  this.sendMessage(aresta.getTargetVertexId(), vertex.getId());
             }
          }


      }
      vertex.voteToHalt();
  }
}
