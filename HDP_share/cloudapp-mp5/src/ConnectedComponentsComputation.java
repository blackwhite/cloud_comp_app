import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.edge.Edge;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;

import java.io.IOException;

/**
 * Implementation of the connected component algorithm that identifies
 * connected components and assigns each vertex its "component
 * identifier" (the smallest vertex id in the component).
 */
public class ConnectedComponentsComputation extends
    BasicComputation<IntWritable, IntWritable, NullWritable, IntWritable> {
  /**
   * Propagates the smallest vertex id to all neighbors. Will always choose to
   * halt and only reactivate if a smaller id has been sent to it.
   *
   * @param vertex Vertex
   * @param messages Iterator of messages from the previous superstep.
   * @throws IOException
   */
  @Override
  public void compute(
      Vertex<IntWritable, IntWritable, NullWritable> vertex,
      Iterable<IntWritable> messages) throws IOException {

      int componenteAtual = vertex.getValue().get();

      if (this.getSuperstep() == 0) {
          int visinho;
          for (Edge<IntWritable, NullWritable> aresta : vertex.getEdges()) {
              visinho = aresta.getTargetVertexId().get();
              if (visinho < componenteAtual) {
                  componenteAtual = visinho;
              }
          }

          if (componenteAtual != vertex.getValue().get()) {
              vertex.setValue(new IntWritable(componenteAtual));
              for (Edge<IntWritable, NullWritable> aresta : vertex.getEdges()) {
                  visinho = aresta.getTargetVertexId().get();
                  if (visinho > componenteAtual) {
                      this.sendMessage(new IntWritable(visinho), vertex.getValue());
                  }
              }
          }
          vertex.voteToHalt();
          return;
      }
      boolean alterado = false;
      int possivelComponente;
      for (IntWritable mensagem : messages) {
          possivelComponente = mensagem.get();

          if (possivelComponente < componenteAtual) {
              componenteAtual = possivelComponente;
              alterado = true;
          }
      }

      if (alterado) {
          vertex.setValue(new IntWritable(componenteAtual));
          this.sendMessageToAllEdges(vertex, vertex.getValue());
      }

      vertex.voteToHalt();

  }
}
