import java.io.IOException;

import org.apache.hadoop.conf.Configuration;

import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;

import org.apache.hadoop.hbase.TableName;

import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;

import org.apache.hadoop.hbase.util.Bytes;

public class SuperTable{

   public static void main(String[] args) throws IOException {

      // Instantiate Configuration class
       Configuration config = HBaseConfiguration.create();

      // Instaniate HBaseAdmin class
       HBaseAdmin adminHbase = new HBaseAdmin(config);
      
      // Instantiate table descriptor class
       HTableDescriptor tableDesc = new HTableDescriptor(TableName.valueOf("powers"));

      // Add column families to table descriptor
       tableDesc.addFamily(new HColumnDescriptor("personal"));
       tableDesc.addFamily(new HColumnDescriptor("professional"));

      // Execute the table through admin
       adminHbase.createTable(tableDesc);

      // Instantiating HTable class
       HTable powersTable = new HTable(config, "powers");

      // Repeat these steps as many times as necessary
       Put personalRow1 = new Put(Bytes.toBytes("row1"));
       Put personalRow2 = new Put(Bytes.toBytes("row2"));
       Put personalRow3 = new Put(Bytes.toBytes("row3"));

       personalRow1.add(Bytes.toBytes("personal"), Bytes.toBytes("hero"), Bytes.toBytes("superman"));
       personalRow1.add(Bytes.toBytes("personal"), Bytes.toBytes("power"), Bytes.toBytes("strength"));
       personalRow1.add(Bytes.toBytes("professional"), Bytes.toBytes("name"), Bytes.toBytes("clark"));
       personalRow1.add(Bytes.toBytes("professional"), Bytes.toBytes("xp"), Bytes.toBytes("100"));

       personalRow2.add(Bytes.toBytes("personal"), Bytes.toBytes("hero"), Bytes.toBytes("batman"));
       personalRow2.add(Bytes.toBytes("personal"), Bytes.toBytes("power"), Bytes.toBytes("money"));
       personalRow2.add(Bytes.toBytes("professional"), Bytes.toBytes("name"), Bytes.toBytes("bruce"));
       personalRow2.add(Bytes.toBytes("professional"), Bytes.toBytes("xp"), Bytes.toBytes("50"));

       personalRow3.add(Bytes.toBytes("personal"), Bytes.toBytes("hero"), Bytes.toBytes("wolverine"));
       personalRow3.add(Bytes.toBytes("personal"), Bytes.toBytes("power"), Bytes.toBytes("healing"));
       personalRow3.add(Bytes.toBytes("professional"), Bytes.toBytes("name"), Bytes.toBytes("logan"));
       personalRow3.add(Bytes.toBytes("professional"), Bytes.toBytes("xp"), Bytes.toBytes("75"));

       powersTable.put(personalRow1);
       powersTable.put(personalRow2);
       powersTable.put(personalRow3);

//       powersTable.close();

//       HTable powersTable2 = new HTable(config, "powers");

       Scan scan = new Scan();

       scan.addColumn(Bytes.toBytes("personal"), Bytes.toBytes("hero"));

       ResultScanner scanner = powersTable.getScanner(scan);

       for (Result resultado = scanner.next(); resultado != null; resultado = scanner.next()) {
           System.out.println("Row: " + resultado);

       }

       scanner.close();
       powersTable.close();



	      // Instantiating Put class
              // Hint: Accepts a row name

      	      // Add values using add() method
              // Hints: Accepts column family name, qualifier/row name ,value

      // Save the table
	
      // Close table

      // Instantiate the Scan class
     
      // Scan the required columns

      // Get the scan result

      // Read values from scan result
      // Print scan result
 
      // Close the scanner
   
      // Htable closer
   }
}

