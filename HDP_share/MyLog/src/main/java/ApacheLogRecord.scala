import java.util

import nl.basjes.parse.core.Field
import nl.basjes.parse.httpdlog.ApacheHttpdLoglineParser
import scala.collection.JavaConverters._

/**
 * Created by unk on 13/10/15.
 */

class ApacheLogRecord {

  var ip: String = ""
  var data: String = ""
  var hora: String = ""
  var metodoHTTP: String = ""
  var url: String = ""
  var codRetorno: String = ""
  var tamanhoResposta: String = ""

  @Field(value = Array("IP:connection.client.host"))
  def setIP(ip: String) {
    this.ip = ip
  }

  @Field(Array("TIME.STAMP:request.receive.time"))
  def setData(data: String): Unit = {
    this.data = data
  }

  def setHora(hora: String): Unit = {
    this.hora = hora
  }

  @Field(Array("HTTP.METHOD:request.firstline.method"))
  def setMetodoHTTP(metodoHTTP: String): Unit = {
    this.metodoHTTP = metodoHTTP
  }

  @Field(Array("HTTP.URI:request.firstline.uri"))
  def setUrl(url: String): Unit = {
    this.url = url
  }

  @Field(Array("STRING:request.status.last"))
  def setCodRetorno(codRetorno: String): Unit = {
    this.codRetorno = codRetorno
  }

  @Field(Array("BYTES:response.body.bytesclf"))
  def setTamanhoResposta(tamResposta: String): Unit = {
    this.tamanhoResposta = tamResposta
  }
}

  object ApacheLogRecord {
    def main(args: Array[String]): Unit = {
      var logFormat = "%h %l %u %t \\\"%r\\\" %>s %b \\\"%{Referer}i\\\" \\\"%{User-Agent}i\\\""

      val record = new ApacheLogRecord
      var simpleParser = new ApacheHttpdLoglineParser[Object](classOf[Object], logFormat)
      val paths: List[String] = simpleParser.getPossiblePaths().asScala.toList

      for (path <- paths) println(path)
      println("OIsss")
    }
  }

