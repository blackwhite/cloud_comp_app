import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.lang.Iterable;


public class PopularityLeague extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new PopularityLeague(), args);
        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = this.getConf();
        FileSystem fs = FileSystem.get(conf);
        Path temp = new Path("/mp2/temp2");
        fs.delete(temp, true);

        Job jobA = new Job(conf, "Links Count");

        jobA.setOutputKeyClass(IntWritable.class);
        jobA.setOutputValueClass(IntWritable.class);

        jobA.setMapOutputKeyClass(IntWritable.class);
        jobA.setMapOutputValueClass(IntWritable.class);

        jobA.setMapperClass(LinkCountMap.class);
        jobA.setReducerClass(LinkCountReduce.class);

        FileInputFormat.setInputPaths(jobA, new Path(args[0]));
        FileOutputFormat.setOutputPath(jobA, temp);
  
        jobA.setJarByClass(PopularityLeague.class);
        jobA.waitForCompletion(true);

        Job jobB = new Job(conf, "TopPopular Links");

        jobB.setOutputKeyClass(IntWritable.class);
        jobB.setOutputValueClass(IntWritable.class);

        jobB.setMapOutputKeyClass(NullWritable.class);
        jobB.setMapOutputValueClass(IntArrayWritable.class);

        jobB.setMapperClass(TopLinkMap.class);
        jobB.setReducerClass(TopLinkReduce.class);

        FileInputFormat.setInputPaths(jobB, temp);
        FileOutputFormat.setOutputPath(jobB, new Path(args[1]));
        jobB.setJarByClass(PopularityLeague.class);

        return jobB.waitForCompletion(true) ? 0 : 1; 
    }

    public static String readFileFromHDFS(String path, Configuration conf) throws IOException {
        Path pt = new Path(path);
        FileSystem fs = FileSystem.get(pt.toUri(), conf);
        FSDataInputStream file = fs.open(pt);
        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(file));

        StringBuilder content = new StringBuilder();
        String line;

        while ((line = bufferReader.readLine()) !=  null) {
            content.append(line);
            content.append("\n");
        }

        return content.toString();
    }

    public static class IntArrayWritable extends ArrayWritable {
        public IntArrayWritable() {
            super(IntWritable.class);
        }

        public IntArrayWritable(Integer[] numbers) {
            super(IntWritable.class);
            IntWritable[] ints = new IntWritable[numbers.length];
            for (int i = 0; i < numbers.length; i++) {
                ints[i] = new IntWritable(numbers[i]);
            }
            set(ints);
        }
    }

  public static class LinkCountMap extends Mapper<Object, Text, IntWritable, IntWritable> {

        @Override
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String links = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(links);
            int page = Integer.parseInt(tokenizer.nextToken(": \\s"));
            int linkTo;

            while (tokenizer.hasMoreTokens()) {
                linkTo = Integer.parseInt(tokenizer.nextToken(": \\s"));
                context.write(new IntWritable(linkTo), new IntWritable(1));
            }

        }
        
    }

    public static class LinkCountReduce extends Reducer<IntWritable, IntWritable, IntWritable, IntWritable> {

        @Override
        public void reduce(IntWritable key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int total = 0;
            for (IntWritable i : values) {
                total += i.get();
            }
            context.write(key, new IntWritable(total));
        }
    }

    public static class TopLinkMap extends Mapper<LongWritable, Text, NullWritable, IntArrayWritable> {
        List<String> leagueIDs;
 
        @Override
        protected void setup(Context context) throws IOException,InterruptedException {
            Configuration conf = context.getConfiguration();

            leagueIDs = Arrays.asList(readFileFromHDFS(conf.get("league"), conf).split("\n"));
        }

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            StringTokenizer tokenizer = new StringTokenizer(value.toString());

            String pg = tokenizer.nextToken();
            int totalLinks = Integer.parseInt(tokenizer.nextToken());

            if (leagueIDs.contains(pg)) 
                context.write(NullWritable.get(), new IntArrayWritable(new Integer[]{Integer.parseInt(pg), totalLinks}));

            
        }
 
    }

    public static class TopLinkReduce extends Reducer<NullWritable, IntArrayWritable, IntWritable, IntWritable> {
        
        private static final Log log = LogFactory.getLog(TopLinkReduce.class);
        TreeSet<Pair<Integer, Integer>> top = new TreeSet<Pair<Integer, Integer>>();

        @Override
        public void reduce(NullWritable key, Iterable<IntArrayWritable> values, Context context) throws IOException, InterruptedException {
            int pgID;
            int total;
            for (IntArrayWritable intArray : values) {
                 IntWritable[] par = (IntWritable[]) intArray.toArray();
                 pgID = par[0].get();
                 total = par[1].get();

                 top.add(new Pair<Integer, Integer>(total, pgID));
            }
            int pos = 0;
            int ant = -1;
            int row = 0;
            Pair<Integer, Integer> par = top.first();
            int atual = par.first;
            context.write(new IntWritable(par.second), new IntWritable(pos));
            ant = par.first;
            top.remove(par);
            log.info("ID:" + par.second + "  - Total:" + par.first + " - POS: " + pos + "Ant:" + ant + "-Atu:" +atual);

            while (!top.isEmpty()) {
                par = top.first();
                atual = par.first;
                log.info("ID:" + par.second + "  - Total:" + par.first + " - POS: " + pos + "Ant:" + ant + "-Atu:" +atual);

                if (atual == ant) {
                    row += 1; 
                    context.write(new IntWritable(par.second), new IntWritable(pos));
                     log.info("===== ID:" + par.second + "  - Total:" + par.first + " - POS: " + pos + "Ant:" + ant + "-Atu:" +atual);
                }
                else if(atual > ant) {
                     log.info("<<<< " + "ID:" + par.second + "  - Total:" + par.first + " - POS: " + pos + "Ant:" + ant + "-Atu:" +atual);
                    pos += 1 + row;
                    context.write(new IntWritable(par.second), new IntWritable(pos));
                    row = 0; 
                }
                ant = atual;
                top.remove(par);
            } 
            
        } // Emitir no cleanUP 
    }

    

}

// >>> Don't Change
class Pair<A extends Comparable<? super A>,
        B extends Comparable<? super B>>
        implements Comparable<Pair<A, B>> {

    public final A first;
    public final B second;

    public Pair(A first, B second) {
        this.first = first;
        this.second = second;
    }

    public static <A extends Comparable<? super A>,
            B extends Comparable<? super B>>
    Pair<A, B> of(A first, B second) {
        return new Pair<A, B>(first, second);
    }

    @Override
    public int compareTo(Pair<A, B> o) {
        int cmp = o == null ? 1 : (this.first).compareTo(o.first);
        return cmp == 0 ? (this.second).compareTo(o.second) : cmp;
    }

    @Override
    public int hashCode() {
        return 31 * hashcode(first) + hashcode(second);
    }

    private static int hashcode(Object o) {
        return o == null ? 0 : o.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Pair))
            return false;
        if (this == obj)
            return true;
        return equal(first, ((Pair<?, ?>) obj).first)
                && equal(second, ((Pair<?, ?>) obj).second);
    }

    private boolean equal(Object o1, Object o2) {
        return o1 == o2 || (o1 != null && o1.equals(o2));
    }

    @Override
    public String toString() {
        return "(" + first + ", " + second + ')';
    }

}
